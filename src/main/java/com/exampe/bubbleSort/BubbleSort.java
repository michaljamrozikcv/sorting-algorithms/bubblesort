package com.exampe.bubbleSort;

import java.util.Random;

class BubbleSort {

    /**
     * złożoność obliczeniowa: O(n^2), w optymistycznym przypadku O(n) gdy tablica jest posortowana
     * złożoność pamięciowa: Omega(1)
     */
    static void bubbleSort(int[] array) {
        boolean swapped = false;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    swapped = true;
                }
            }//once array is sorted, time complexity is n, outer loop is broken
            if (!swapped) {
                break;
            }
        }
    }

    /**
     * Return array of random integers
     *
     * @param size   the number of values to generate
     * @param origin the origin (inclusive) of each random value
     * @param bound  the bound (exclusive) of each random value
     */
    static int[] createRandomArray(int size, int origin, int bound) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.ints(1, origin, bound).findFirst().getAsInt();
        }
        return array;
    }
}
