package com.exampe.bubbleSort;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = BubbleSort.createRandomArray(20, -5, 5);
        System.out.println("Array before sorting:");
        System.out.println(Arrays.toString(array));
        System.out.println("Sorted array:");
        BubbleSort.bubbleSort(array);
        System.out.println(Arrays.toString(array));
    }
}
